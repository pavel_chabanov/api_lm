import "isomorphic-fetch";
import config from "./config/index";
import db from "./core/db";
import s from "./core/server";

(async () => {
    try {
        await db(`mongodb://${config.db.host}/${config.db.name}`);
        console.log(`Database connected NN`);
        await s.listen(config.server.port, config.server.host, async () => {
            console.log(`Server start ${config.server.alias}`);
            console.log(s.address());
        });
    } catch (error) {
        console.error(`Error start SERVER => : `, error);
    }
})();