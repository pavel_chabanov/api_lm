export default class PermissionController {
    constructor(roles){
        this._roles = roles;
        this.init = this.init.bind(this);
    }

    async init(req, res, next){
        try {
            return await next()
        } catch (e) {
            throw next(e)
        }
    }
}