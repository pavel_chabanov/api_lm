import mongoose from "mongoose";
import timestamps from 'mongoose-timestamp';
import mongooseStringQuery from 'mongoose-string-query';
import autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const ConfigModel = new Schema({
    name: {
        type: String,
        required: true
    },
    logo: {
        type: String,
        required: true
    }
});

ConfigModel.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        delete ret._id
    }
});

ConfigModel.plugin(timestamps, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

ConfigModel.plugin(mongooseStringQuery);
ConfigModel.plugin(autopopulate);

export default mongoose.model('Config', ConfigModel);