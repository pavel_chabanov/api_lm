import License from "./model";
import Config from "./model.config";

class LicenseController {

    static async create(req, res, next){
        try {
            const NewConfig = new Config({
                name: req.body.name,
                logo: req.body.logo
            });
            const SaveConfig = await NewConfig.save();
            const NewsLicense = new License({
                landing: req.body.landing,
                config: SaveConfig.id,
                name: req.body.name
            });
            return await res.json(await NewsLicense.save())
        } catch (e) {
            throw next(e)
        }
    }

    static async getByKey(req, res, next){
        try {
            const FindLicense = await License.findOne({license_key: req.params.key});
            return await res.json(FindLicense)
        } catch (e) {
            throw next(e)
        }
    }
}

export default LicenseController