import LicenseController from "./controller";

const LicenseRouter = [];

LicenseRouter.push({
    name: 'Create new license',
    method: 'post',
    roles: ['ANONYMOUS', 'USER', 'ADMIN'],
    uri: '/license',
    ctrl: LicenseController.create
});

LicenseRouter.push({
    name: 'Get landing to license key',
    method: 'get',
    roles: ['ANONYMOUS', 'USER', 'ADMIN'],
    uri: '/license/:key',
    ctrl: LicenseController.getByKey
});

export default LicenseRouter;