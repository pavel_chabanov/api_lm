import mongoose from "mongoose";
import timestamps from 'mongoose-timestamp';
import mongooseStringQuery from 'mongoose-string-query';
import autopopulate from 'mongoose-autopopulate';
import {uidGen} from "../../utils/index";

const Schema = mongoose.Schema;

const LicenseModel = new Schema({
    // buyer: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'User',
    //     required: true,
    //     autopopulate: true,
    // },
    landing: {
        type: Schema.Types.ObjectId,
        ref: 'Landing',
        required: true,
        autopopulate: true,
    },
    name: {
        type: String,
        required: true
    },
    license_key: {
        type: String,
        unique: true,
        default: () => uidGen(16)
    },
    config: {
        type: Schema.Types.ObjectId,
        ref: 'Config',
        required: true,
        autopopulate: true,
    }
});

LicenseModel.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        delete ret._id
    }
});

LicenseModel.plugin(timestamps, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

LicenseModel.plugin(mongooseStringQuery);
LicenseModel.plugin(autopopulate);

export default mongoose.model('License', LicenseModel);