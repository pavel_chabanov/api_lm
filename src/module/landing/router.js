import LandingController from "./controller";

const LandingRouter = [];

LandingRouter.push({
    name: 'Get all landings',
    method: 'get',
    roles: ['ANONYMOUS', 'USER', 'ADMIN'],
    uri: '/landings',
    ctrl: LandingController.getAll
});

LandingRouter.push({
    name: 'Create new landing',
    method: 'post',
    roles: ['USER', 'ADMIN'],
    uri: '/landing',
    ctrl: LandingController.create
});

export default LandingRouter;