import LandingController from "./controller";
import LandingRouter from "./router";
import LandingModel from "./model";

const Landing = {
  controller: LandingController,
  model: LandingModel,
  router: LandingRouter
};

export default Landing;