import mongoose from "mongoose";
import timestamps from 'mongoose-timestamp';
import mongooseStringQuery from 'mongoose-string-query';
import autopopulate from 'mongoose-autopopulate';
import {uidGen} from "../../utils";

const Schema = mongoose.Schema;

const LandingModel = new Schema({
    price: {
        type: Number,
        required: true,
        default: 0
    },
    version: {
        type: String,
        required: true,
        default: '0.0.1'
    },
    publish: {
        type: Boolean,
        required: true,
        default: true
    },
    preview: {
        type: String,
        required: true,
        default: 'https://preview.ozi.io'
    },
    downloads: {
        type: Number,
        required: true,
        default: 0
    },
    link_demo: {
        type: String,
        required: true,
        default: 'https://ozi.io'
    },
    name: {
        type: String,
        required: true
    },
    style: {
        type: String,
        required: true,
        default: `body{background: #f2f2f2;}`
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true,
        autopopulate: true,
    },
    description: {
        type: String,
        required: true
    },
    landing_key: {
        type: String,
        unique: true,
        default: () => uidGen(16)
    },
});

LandingModel.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        delete ret._id
    }
});

LandingModel.plugin(timestamps, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

LandingModel.plugin(mongooseStringQuery);
LandingModel.plugin(autopopulate);

export default mongoose.model('Landing', LandingModel);