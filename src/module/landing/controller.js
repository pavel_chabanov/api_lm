import LandingModel from "./model";

class LandingController {
    static async create(req, res, next){
        try {
            const NewModel = new LandingModel({
                category: req.body.category,
                name: req.body.name,
                description: req.body.description,
                preview: req.body.preview
            });
            return await res.json(await NewModel.save())
        } catch (e) {
            throw next(e)
        }
    }
    static async getAll(req, res, next){
        try {
            const ArrayDate = await LandingModel.find();
            return await res.json(ArrayDate.reverse())
        } catch (e) {
            throw next(e)
        }
    }
}

export default LandingController