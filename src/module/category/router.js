import CategoryController from "./controller";

const CategoryRouter = [];

CategoryRouter.push({
    name: 'Create category',
    method: 'post',
    roles: ['ANONYMOUS', 'USER', 'ADMIN'],
    uri: '/category',
    ctrl: CategoryController.create
});

CategoryRouter.push({
    name: 'Get all categories',
    method: 'get',
    roles: ['ANONYMOUS', 'USER', 'ADMIN'],
    uri: '/categories',
    ctrl: CategoryController.getAll
});

export default CategoryRouter;