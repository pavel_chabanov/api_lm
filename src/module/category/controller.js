import Category from "./model";

class CategoryController {

    static async create(req, res, next){
        try {
            const NewModel = new Category({
                name: req.body.name,
                description: req.body.description
            });
            return await res.json(await NewModel.save())
        } catch (e) {
            throw next(e)
        }
    }

    static async getAll(req, res, next){
        try {
            const ArrayData = await Category.find();
            return await res.json(ArrayData.reverse())
        } catch (e) {
            throw next(e)
        }
    }
}

export default CategoryController