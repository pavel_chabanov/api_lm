import mongoose from "mongoose";
import timestamps from 'mongoose-timestamp';
import mongooseStringQuery from 'mongoose-string-query';
import autopopulate from 'mongoose-autopopulate';

const Schema = mongoose.Schema;

const CategoryModel = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
});

CategoryModel.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: (doc, ret) => {
        delete ret._id
    }
});

CategoryModel.plugin(timestamps, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});

CategoryModel.plugin(mongooseStringQuery);
CategoryModel.plugin(autopopulate);

export default mongoose.model('Category', CategoryModel);