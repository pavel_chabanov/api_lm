import LicenseController from "./controller";
import LicenseRouter from "./router";
import LicenseModel from "./model";

const License = {
  controller: LicenseController,
  model: LicenseModel,
  router: LicenseRouter
};

export default License;