import PermissionController from "../permission/PermissionController";

const BigRouter = [
    ...require("../module/landing").default.router,
    ...require("../module/license").default.router,
    ...require("../module/category").default.router
];

export default (Router) => {
    const router = new Router();
    BigRouter.forEach(route => {
        const RolesPermissionCtrl = new PermissionController(route.roles);
        return router[route.method](
            route.uri,
            RolesPermissionCtrl.init,
            route.ctrl,
        )
    });
    return router;
}