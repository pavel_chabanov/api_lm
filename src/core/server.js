import helmet from "helmet";
import compression from "compression";
import express, {Router} from "express";
import cors from "cors";
import morgan from "morgan";
import methodOverride from "method-override";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import {Server} from "http";
import useragent from "express-useragent";
import responseTime from "response-time";
import Routes from "../routes/index";
import {ApplicationError, uidGen} from "../utils/index";
import errorhandler from "errorhandler";
import session from "express-session";

const app = express();
const server = Server(app);
const io = require('socket.io')(server);

app.use(compression());
app.use(helmet());
app.use(helmet.hidePoweredBy({setTo: 'CORE SERVER'}));
app.use(helmet.noSniff());
app.use(helmet.ieNoOpen());
app.use(helmet.dnsPrefetchControl({allow: false}));
app.use(cors({origin: '*'}));
app.use(cookieParser());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(session({ secret: 'conduit', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false  }));
app.use(errorhandler());
app.use(useragent.express());
app.use(responseTime());

// app.use(async (req, res, next) => {
//     console.log(req);
//     return await next()
// });

// app.use(function(err, req, res, next) {
//     if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
//         console.error('Bad JSON');
//     }
// });

app.use(async (req, res, next) => {
    res.setHeader('Ozi-Xss-Key', 'ON');
    res.setHeader('Ozi-Key', `${uidGen(8)}`);
    res.setHeader('Ozi-Hash-Key', `${uidGen(16)}`);
    res.setHeader('Ozi-Verify-Key', `${uidGen(64)}`);
    res.setHeader('Ozi-Request-Key', `${uidGen(22)}-${uidGen(4)}`);
    res.setHeader('Ozi-Response-Key', `${uidGen(22)}-${uidGen(4)}`);
    return await next();
});

app.use(async (req, res, next) => {
    req.io = await io;
    return await next();
});

// app.use(async (req, res, next) => {
//     if (!req.headers['authorization']) throw next(new ApplicationError("Unauthorized", 401));
//     return await next();
// });

app.use('/v1', Routes(Router));

//Not Found
app.use(async (req, res) => await res.status(404).json({
    message: `NOT FOUND`,
    status: 404
}));

//Global next Error
app.use(async (err, req, res, next) => {
    console.log(err);
    return await res.status(
        err.status
            ? err.status
            : 500
    ).json(err);
});

export default server;