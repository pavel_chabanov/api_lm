import {polyfill} from "es6-promise";
import mongoose from "mongoose";

polyfill();

mongoose.Promise = Promise;

mongoose.set('debug', true);

export default function (uri) {
    return new Promise((resolve, reject) => {
        mongoose.connection
            .on('error', error => reject(error))
            .on('close', () => console.log('Database connection closed.'))
            .once('open', () => resolve(mongoose.connections[0]));
        mongoose.connect(uri);
    });
}